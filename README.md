![Logo](https://i.imgur.com/PyKLAe7.png)

[![License](https://img.shields.io/badge/license-Public_domain-red.svg)](https://wiki.creativecommons.org/wiki/Public_domain)

About
----

**IPsum** is a threat intelligence feed based on 30+ different publicly available [lists](https://github.com/stamparm/maltrail) of suspicious and/or malicious IP addresses. All lists are automatically retrieved and parsed on a daily (24h) basis and the final result is pushed to this repository. List is made of IP addresses together with a total number of (black)list occurrence (for each). Greater the number, lesser the chance of false positive detection and/or dropping in (inbound) monitored traffic. Also, list is sorted from most (problematic) to least occurent IP addresses.

As an example, to get a fresh and ready-to-deploy auto-ban list of "bad IPs" that appear on at least 3 (black)lists you can run:

```
curl --compressed https://raw.githubusercontent.com/stamparm/ipsum/master/ipsum.txt 2>/dev/null | grep -v "#" | grep -v -E "\s[1-2]$" | cut -f 1
```

If you want to try it with `ipset`, you can do the following:

```
sudo su
apt-get -qq install iptables ipset
ipset -q flush ipsum
ipset -q create ipsum hash:net
for ip in $(curl --compressed https://raw.githubusercontent.com/stamparm/ipsum/master/ipsum.txt 2>/dev/null | grep -v "#" | grep -v -E "\s[1-2]$" | cut -f 1); do ipset add ipsum $ip; done
iptables -I INPUT -m set --match-set ipsum src -j DROP
```

In directory [levels](levels) you can find preprocessed raw IP lists based on number of blacklist occurrences (e.g. [levels/3.txt](levels/3.txt) holds IP addresses that can be found on 3 or more blacklists).

**Important:** If you are planning to use `git` to get the content of this repository do it like `git clone --depth 1 https://github.com/stamparm/ipsum.git`

Wall of shame (2019-01-08)
----

|IP|DNS lookup|Number of (black)lists|
|---|---|--:|
178.73.215.171|178-73-215-171-static.glesys.net|10
171.25.193.20|tor-exit0-readme.dfri.se|10
58.42.228.170|-|9
218.5.36.120|-|9
27.3.150.15|-|9
122.2.223.242|122.2.223.242.static.pldt.net|9
185.244.25.157|-|8
123.127.87.37|-|8
211.184.37.117|-|8
218.52.62.122|-|8
89.234.157.254|marylou.nos-oignons.net|8
145.249.104.196|-|8
171.25.193.25|tor-exit5-readme.dfri.se|8
165.227.10.194|-|8
45.119.212.105|-|8
197.231.221.211|exit1.ipredator.se|8
65.19.167.131|-|8
211.201.237.99|-|8
185.244.25.145|-|8
119.4.250.72|-|8
23.129.64.101|yawnbox.emeraldonion.org|8
35.0.127.52|tor-exit.eecs.umich.edu|8
64.113.32.29|tor.t-3.net|8
31.202.199.114|31.202.199.114.format-tv.net|8
201.144.84.82|static.customer-201-144-84-82.uninet-ide.com.mx|8
171.25.193.77|tor-exit1-readme.dfri.se|8
171.25.193.78|tor-exit4-readme.dfri.se|8
222.236.67.150|-|8
89.31.57.5|dreamatorium.badexample.net|8
27.104.197.239|239.197.104.27.unknown.m1.com.sg|8
59.30.101.105|-|8
125.64.94.200|-|8
87.228.111.210|-|8
80.82.77.139|dojo.census.shodan.io|8
62.102.148.67|-|8
202.29.239.21|-|8
74.95.138.121|74-95-138-121-Miami.hfc.comcastbusiness.net|8
82.200.168.82|82.200.168.82.adsl.online.kz|8
125.77.30.213|-|8
209.20.72.246|filmagenda.it|8
122.114.223.55|-|8
198.96.155.3|exit.tor.uwaterloo.ca|8
185.165.168.229|-|8
185.220.101.28|-|8
112.161.175.197|-|8
112.254.116.143|-|8
121.17.18.219|-|8
106.12.81.245|-|8
58.135.224.45|-|8
218.48.59.189|-|8
